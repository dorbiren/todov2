import { Command, Flag, ParseResult } from "./types.js";

export function CliTodosValidate(parseResult: ParseResult): boolean {
    const [commands, flags, args] = [
        parseResult.commandes,
        parseResult.flags,
        parseResult.args,
    ];

    if (!onlyOneCommand(commands))
        throw new Error("only one command is allowed!");
    if (!onlyOneFlag(flags)) throw new Error("only one Flag is allowed!");
    if (!commandsExist(commands))
        throw new Error(`command ${commands[0]} is not exist!`);
    if (!FlagsExist(flags)) throw new Error(`flag ${flags[0]} is not exist!`);

    return true;
}

const onlyOneCommand = (commands: string[]): boolean => commands.length === 1;
const onlyOneFlag = (flags: string[]): boolean => flags.length <= 1;

const commandsExist = (commands: string[]): boolean => {
    return !!commands.reduce(
        (prev, cmd) => prev && Object.values(Command).includes(cmd as Command),
        true
    );
};
const FlagsExist = (flags: string[]): boolean => {
    return !!flags.reduce(
        (prev, flag) => prev && Object.values(Flag).includes(flag as Flag),
        true
    );
};
