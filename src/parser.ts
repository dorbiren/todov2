import { ParseResult } from "./types.js";

export function parse(input: string[]): ParseResult {
    input = input.slice(2);
    const commandes = [];
    const flags = [];
    const args = [];

    for (const item of input) {
        item.substring(0, 2) === "--"
            ? commandes.push(item.slice(2))
            : item.substring(0, 1) === "-"
            ? flags.push(item.slice(1))
            : args.push(item);
    }

    return { commandes, flags, args };
}
