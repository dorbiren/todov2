import fs from "fs/promises";
import { Todo } from "./types.js";

export class Model {
    constructor(private DBpath: string, private DBname: string) {
        this.DBpath = DBpath;
        this.DBname = DBname;
    }

    async load(): Promise<Todo[]> {
        const dirContent = await fs.readdir(this.DBpath);
        const exist: boolean = dirContent.includes(this.DBname);
        const data = exist
            ? await fs.readFile(`${this.DBpath}/${this.DBname}`, "utf8")
            : "[]";
        return JSON.parse(data) as Todo[];
    }

    async save(todos: Todo[]) {
        try {
            const data = JSON.stringify(todos);
            await fs.writeFile(`${this.DBpath}/${this.DBname}`, data);
        } catch (err) {
            console.error(err);
        }
    }
}
