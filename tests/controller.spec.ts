import { expect } from "chai";
import { Contoller } from "../src/controller";
import fs from "fs/promises";
import { before } from "mocha";
import { Todo } from "../src/types.js";


describe("The Controller module", () => {
    const C = new Contoller();
    const t1: Todo = { description: "task1", completed: false, id: "id1" };
    const t2: Todo = { description: "task2", completed: false, id: "id2" };
    const t3: Todo = { description: "task3", completed: false, id: "id3" };

   /*  beforeEach(() => {
        fs.writeFile(`${process.cwd()}/${"testDB.json"}`, "[]");
    });
 */
    context("#removeFromList", () => {
        it("should exist", () => {
            expect(C.removeFromList).to.be.a("function");
        });

        it("should load empty tasks array", async () => {
           
            expect(C.removeFromList([t1,t2,t3],"id2")).to.eql([t1,t3]);
        });

    });

    context("#toggleCompleted", () => {
        it("should exist", () => {
            expect(C.toggleCompleted).to.be.a("function");
        });

        it("should load empty tasks array", async () => {
           
            const tasksList =  [t1,t2,t3];
            const beforeToggle = t2.completed;
            C.toggleCompleted(tasksList,"id2");
            const afterToggle = t2.completed;
            expect( afterToggle !== beforeToggle).to.be.true;
        });
        
    });
    
    
    
    context("#addTodo", () => {
        const C = new Contoller();

        it("should exist", () => {
            expect(C.addTodo).to.be.a("Function");
        });

        it("should add new todo to the list", () => {
            const todos = [t1];
            C.addTodo(todos, "test test test");
            expect(todos.length).to.equals(2);
            const newTaskDesc = todos[1].description;
            expect(newTaskDesc).to.equal("test test test");
        });

        it("new task should have be not completed", () => {
            const C = new Contoller();
            const todos = [t1];
            C.addTodo(todos, "test test test");
            expect(todos.length).to.equals(2);
            const newTaskComp = todos[1].completed;
            expect(newTaskComp).to.equal(false);
        });
    });

    context("#addTodo", () => {
        const C = new Contoller();

        it("should exist", () => {
            expect(C.addTodo).to.be.a("Function");
        });

        it("should add new todo to the list", () => {
            const todos = [t1];
            C.addTodo(todos, "test test test");
            expect(todos.length).to.equals(2);
            const newTaskDesc = todos[1].description;
            expect(newTaskDesc).to.equal("test test test");
        });

        it("new task should have be not completed", () => {
            const C = new Contoller();
            const todos = [t1];
            C.addTodo(todos, "test test test");
            expect(todos.length).to.equals(2);
            const newTaskComp = todos[1].completed;
            expect(newTaskComp).to.equal(false);
        });
    });
});
