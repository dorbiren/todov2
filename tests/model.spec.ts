import { expect } from "chai";
import { Model } from "../src/model";
import fs from "fs/promises";
import { before } from "mocha";
import { Todo } from "../src/types.js";


describe("The Model module", () => {
    const M = new Model(process.cwd(), "testDB.json");
    const t1: Todo = { description: "task1", completed: false, id: "id1" };
    const t2: Todo = { description: "task2", completed: false, id: "id2" };
    const t3: Todo = { description: "task3", completed: false, id: "id3" };

    beforeEach(() => {
        fs.writeFile(`${process.cwd()}/${"testDB.json"}`, "[]");
    });

    context("#load", () => {
        it("should exist", () => {
            expect(M.load).to.be.a("function");
        });

        it("should load empty tasks array", async () => {
            const tasks = await M.load();
            expect(tasks).to.eql([]);
        });
    });
    context("#load with values", () => {

        it("should load tasks array", async () => {
            await fs.writeFile(`${process.cwd()}/${"testDB.json"}`,JSON.stringify([t1,t2,t3]));
            const tasks = await M.load();
            expect(tasks.length).to.equal(3);
        });
    });

    context("#save", () => {
      
        it("should exist", () => {
            expect(M.save).to.be.a("function");
        });
        it("should exist2", () => {
            expect(M.save).to.be.a("function");
        });

        it("should write to-do list to DB",async () => {
           
           await M.save([{id :"0", description :"hello world",completed : false} ]);
           const rawData = await fs.readFile(`${process.cwd()}/${"testDB.json"}`,"utf8");
           
           //parse data from db and compare to the given list
           expect(await JSON.parse(rawData)).
           to.eql([{id :"0", description :"hello world",completed : false}]);
           
        });

        
    });
});
